@extends('layouts.staradmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card my-3">
                <div class="card-body">
                <div class="card-title">Dashboard</div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>

            <example-component></example-component>

            <div class="card border-info my-3">
                <div class="card-body text-info">
              <div class="card-title">Header</div>
                <h5 class="card-title">Info card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              </div>
            </div>

        </div>
    </div>
</div>
@endsection
